try{
    var payload = JSON.parse(context.getVariable("request.content"));
    var roles = payload.roles;
    for(var i = 0; i < roles.length; i++){
        if(roles[i].dataAssetKey == "BILLING_ID"){
            if(roles[i].dataAssetValue.length < 4){
                context.setVariable("errorMessage", "Please provide a valid billing id.");
                context.setVariable("isValidRequest", false);
                break;
            }
        }
    }
}
catch(exception){
    print("Request was not json");
}
