var email = context.getVariable("request.queryparam.email");
var cdgId = context.getVariable("request.queryparam.cdgId");
context.setVariable("isValidRequest", true);

var emailRegex = new RegExp(/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/);

if(!emailRegex.test(email)){
    context.setVariable("isValidRequest", false);
    context.setVariable("errorMessage", "Please provide a valid email");
    context.setVariable("paramName", "email");
    print("got invalid email");
}

if(cdgId === "" || cdgId === null){
    context.setVariable("isValidRequest", false);
    context.setVariable("errorMessage", "Please provide a cdgId");
    context.setVariable("paramName", "cdgId");
}